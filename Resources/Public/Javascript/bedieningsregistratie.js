// functies voor bedieningsregistratie
// variabelen voor contextmenu:
var menuElement, menusoort, treeid

var draggingElmId;
draggingElmId = 0;
draggingsoort='';
var IsDragging;
IsDragging=0;


function toggle(ide,alleenuitklappen)
{	
	var mm=$("#"+ide+"_toggle");
	var li=$("#"+ide+"_li");
	var oc=$("#"+ide+"_drag");
	var ul=$("#"+ide);
	var soort=ide.split('_')[1];
	var soortenkey=["a","b","t","m"];
	var soorten=["closed","blueclosed","taak","mdw"];
	var soortenopen=["open","blueopen","taak","mdw"];
	var srt=soortenkey.indexOf(soort);
	var imageopen=soortenopen[srt];
	var imageclosed=soorten[srt];
	var nuopen=mm.hasClass("min");
	if (mm.hasClass("eind") && !alleenuitklappen) return;
	if (nuopen) {
		// open, dus sluiten:
		oc.removeClass(imageopen);
		oc.addClass(imageclosed);
		if (ul.has("li").length) mm.attr("class","plus"); 
		else mm.attr("class","eind");
		ul.css("display","none");
	}
	if (!nuopen || alleenuitklappen) {
		// plus-> open klappen:
		oc.removeClass(imageclosed);
		oc.addClass(imageopen);
		if (ul.has("li").length) 
		{
			mm.attr("class","min");
			ul.css("display","block");
		}
		else mm.attr("class","eind");
	}
}	

//function tDragStart(ide) {
function tDragStart(evt) {
   	if (!evt) var evt = window.event;
	var relTarg = evt.target || evt.srcElement;
	//alert("event:"+show_props(evt,"event"));
	draggingElmId = relTarg.id;
	IsDragging=0;
	var aid=relTarg.id.split("_");
	//draggingElmId=aid[0]+"_"+aid[1];
	draggingsoort=aid[1];
	var drgElm =document.getElementById(draggingElmId);
	//alert("draggingElmId:"+draggingElmId+" draggable:"+drgElm.draggable);
	if (!drgElm.draggable)draggingElmId=0;  // skip if not dragging allowed
}

function show_props(obj, objName) {
	   var result = "";
	   for (var i in obj) {
	      result += objName + "." + i + " = " + obj[i] + "\n";
	   }
	   return result;
	} 
function tDragEnter(evt) {
	if (draggingElmId !== 0)
	{	IsDragging=1;
		tDragOver(evt);
	}
}
function tDragOver(evt) {
	if (draggingElmId !== 0 && IsDragging==1)
   	{	if (!evt) var evt = window.event;
		var relTarg = evt.target || evt.srcElement;
		if (relTarg.id)
		{	var ide=relTarg.id;
			var aid=ide.split("_");
			if (aid.length==3 && (aid[2]=="drag"||aid[2]=="drop"))
			{
				//alert("ide:"+ide+"; prevent:"+evt.preventDefault);
				var eig=false;
				var srcElm =document.getElementById(relTarg.parentNode.parentNode.id);
				// niet droppen op eigen child:
				if (srcElm.draggable && srcElm.parentNode.id!==draggingElmId) 
				{	var aid=ide.split("_");
					var soort=aid[1];
					if ((draggingsoort=="m" && soort=="t" ||
							draggingsoort!="m" && soort=="a" ||
							draggingsoort=="t" && soort=="b"))
					{
						if (evt.preventDefault){evt.preventDefault();evt.stopPropagation();} else {evt.returnValue = false;evt.cancelBubble = true;}
					}
				}
			}
		} 
	}
	return false;
}

function tDrop(evt) {
	//alert("Drop IsDragging:"+window.IsDragging+":"+draggingElmId);
	if (draggingElmId !== 0 && window.IsDragging==1)
	{  	if (!evt) var evt = window.event;
		var relTarg = evt.target || evt.srcElement;
		var aid=relTarg.id.split("_");
		var destId = aid[0]+"_"+aid[1];
		destElm  = $("#"+destId);
		if (destElm)
		{	
			dragElm=$("#"+draggingElmId);
			aold=dragElm.parent().attr('id').split('_');
			dragElm.appendTo(destElm)
			toggle(aold[0]+'_'+aold[1],true);
			toggle(destElm.attr('id'),true);
		} 
		evt.cancelBubble = true;
		if (evt.stopPropagation) evt.stopPropagation();
		// start php voor vastleggen drag-drop:
		var dataElm,orgId;
		adrag =draggingElmId.split("_");
		orgId=adrag[0];
		//alert ("dropping :"+orgId+" op: "+destId+" met soort:"+draggingsoort);
		
		$.ajax({
			method: "POST",
			url: ministryaction,
			data: {
					"config": myconfig,
                    "action": "droptaak",
					"id": orgId,
                    "destId": destId,
					"soort": draggingsoort,
				},
			success: function(result,status,xhr){
				//alert("changenewname:"+changenewname);
			}, 
			error: function (result, statusText, jqXHR) {
	            alert("Error: ("+statusText+")" + jqXHR );
			},
		}); 
	}
	draggingElmId=0;
	IsDragging=0;
}





