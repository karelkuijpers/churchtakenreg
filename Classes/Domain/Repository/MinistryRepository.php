<?php

namespace Parousia\Churchtakenreg\Domain\Repository;
ini_set("display_errors",1);
ini_set("log_errors",1);

/**
 * Class MinistryRepository
 *
 * package Parousia\Churchtakenreg\Domain\Repository
 *
 * return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
 */
class MinistryRepository extends AbstractRepository
{
	var $ErrMsg=""; //error message
	var $personReg=false;
	var $ministryReg=false;
	var $userid="";
	var $query;
	var $db;
	protected $aExport;

	
	public function findHierarchy(string $omschrijving = null, int $id_parent = null)
	{
		$this->initializeObject();
		$query = $this->createQuery();
                $statement = "SELECT `omschrijving`, `uid` FROM `bediening` WHERE `omschrijving` LIKE '".$omschrijving."%' ";
                if ($id_parent) {
                $statement .= " AND `id_parent` = $id_parent ";
                } 
                $statement .= "ORDER BY `omschrijving` Desc LIMIT 1";
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findBediening statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

                $query->statement($statement);
                $result= $query->execute(true);
                return $result[0];

        }


	
}