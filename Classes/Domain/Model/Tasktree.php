<?php
namespace Parousia\Churchtakenreg\Domain\Model;

/***
 *
 * This file is part of the "Churchtakenreg" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * 
 */
class Tasktree extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * @var int
     */
    protected $uid = null;
	
    /**
     * the omschrijving of an taak
     *
     * @var string
    */
	protected $omschrijving ='';
	
	/**
     * the workers for this task
     *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Parousia\Churchtakenreg\Domain\Model\Workertree>
	*/
    protected $workers;

    /**
     * Initialize workerree
     *
     * @return \Parousia\Churchtakenreg\Domain\Model\Workertree
     */
    public function __construct()
    {
        $this->workers = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

   /**
     * Returns the uid
     *
     * @return int $uid
     */
    public function getUid():?int
    {
        return (int)$this->uid;
    }

    /**
     * Sets the uid
     *
     * @param int $uid
     * @return void
    */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

   /**
     * Returns the omschrijving
     *
     * @return string $omschrijving
    */
    public function getOmschrijving()
    {
        return $this->omschrijving;
    }
	/**
     * Sets the omschrijving
     *
     * @param string $omschrijving
     * @return void
    */
    public function setOmschrijving($omschrijving)
    {
        $this->omschrijving = $omschrijving;
    }

	/**
     * Returns the workers
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getWorkers()
    {
        return $this->workers;
    }

	/**
     * Add a worker reference
     *
     * @param \Parousia\Churchtakenreg\Domain\Model\Workertree $workertree
     */
    public function addWorker(\Parousia\Churchtakenreg\Domain\Model\Workertree $workertree)
    {
        if ($this->getWorkers() === null) {
            $this->workers = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        }
        $this->workers->attach($workertree);
    }


}
