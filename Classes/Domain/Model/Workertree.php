<?php
namespace Parousia\Churchtakenreg\Domain\Model;

/***
 *
 * This file is part of the "Churchtakenreg" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * 
 */
class Workertree extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * @var int
     */
    protected $uid = null;
	
    /**
     * the fullname of this coworker
     *
     * @var string
    */
	protected $fullname ='';
	
   /**
     * Returns the uid
     *
     * @return int $uid
     */
    public function getUid():?int
    {
        return (int)$this->uid;
    }

    /**
     * Sets the uid
     *
     * @param int $uid
     * @return void
    */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }
	
   /**
     * Returns the fullname
     *
     * @return string $fullname
    */
    public function getFullname()
    {
        return $this->fullname;
    }
	/**
     * Sets the fullname
     *
     * @param string $fullname
     * @return void
    */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }


}
