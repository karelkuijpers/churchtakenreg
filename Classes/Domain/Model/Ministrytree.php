<?php
namespace Parousia\Churchtakenreg\Domain\Model;

/***
 *
 * This file is part of the "Ministryreg" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * 
 */
class Ministrytree extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * @var int
     */
    protected $uid = null;
	
    /**
     * the omschrijving of an ministry
     *
     * @var string
    */
	protected $omschrijving ='';
	
    /**
     * the soort of an ministry
     *
     * @var string
    */
	protected $soort ='';
	
    /**
     * the indicator this is a ministry of the user
     *
     * @var int
    */
	protected $eigen ='';
	
	/**
     * the subministries of this ministry
     *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Parousia\Churchtakenreg\Domain\Model\Ministrytree>
	*/
    protected $subministries;

	/**
     * the tasks of this ministry
     *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Parousia\Churchtakenreg\Domain\Model\Tasktree>
	*/
    protected $tasks;

    /**
     * Initialize ministrytree
     *
     * @return \Parousia\Churchtakenreg\Domain\Model\Ministrytree
     */
    public function __construct()
    {
        $this->subministries = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->tasks = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

   /**
     * Returns the uid
     *
     * @return int $uid
     */
    public function getUid():?int
    {
        return (int)$this->uid;
    }

    /**
     * Sets the uid
     *
     * @param int $uid
     * @return void
    */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

   /**
     * Returns the omschrijving
     *
     * @return string $omschrijving
    */
    public function getOmschrijving()
    {
        return $this->omschrijving;
    }
	/**
     * Sets the omschrijving
     *
     * @param string $omschrijving
     * @return void
    */
    public function setOmschrijving($omschrijving)
    {
        $this->omschrijving = $omschrijving;
    }

   /**
     * Returns the eigen
     *
     * @return int $eigen
    */
    public function getEigen()
    {
        return $this->eigen;
    }
	/**
     * Sets the eigen
     *
     * @param int $eigen
     * @return void
    */
    public function setEigen($eigen)
    {
        $this->eigen = $eigen;
    }

   /**
     * Returns the soort
     *
     * @return string $soort
    */
    public function getSoort()
    {
        return $this->soort;
    }
   /**
     * Returns the soort truncated
     *
     * @return string $soort
    */
    public function getSoortT()
    {
        return substr($this->soort,0,1);
    }
	/**
     * Sets the soort
     *
     * @param string $soort
     * @return void
    */
    public function setSoort($soort)
    {
        $this->soort = $soort;
    }
	
	/**
     * Returns the subministries
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getSubministries()
    {
        return $this->subministries;
    }

	/**
     * Add a ministrytree reference
     *
     * @param \Parousia\Churchtakenreg\Domain\Model\Ministrytree $ministrytree
     */
    public function addSubministry(\Parousia\Churchtakenreg\Domain\Model\Ministrytree $ministrytree)
    {
        if ($this->getSubministries() === null) {
            $this->subministries = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        }
        $this->subministries->attach($ministrytree);
    }
	
	/**
     * Returns the tasks
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getTasks()
    {
        return $this->tasks;
    }

	/**
     * Add a tasktree reference
     *
     * @param \Parousia\Churchtakenreg\Domain\Model\Tasktree $tasktree
     */
    public function addTask(\Parousia\Churchtakenreg\Domain\Model\Tasktree $tasktree)
    {
        if ($this->gettasks() === null) {
            $this->tasks = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        }
        $this->tasks->attach($tasktree);
    }



}
