<?php
namespace Parousia\Churchtakenreg\Domain\Model;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/***
 *
 * This file is part of the "Ministryreg" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * 
 */
class Ministry extends AbstractEntity
{
    /**
     * the composed names of leaders of an ministry
     *
     * @var string
    */
	protected $bedieningsleiders ='';
	
    /**
     * the composed names of secretaries of an ministry
     *
     * @var string
    */
	protected $secretarissen ='';
	
    /**
     * the composed ids of leaders of an ministry
     *
     * @var string
    */
	protected $id_bedieningsleider='';
	
    /**
     * the composed ids of secretaries of an ministry
     *
     * @var string
    */
	protected $id_secretarissen='';
	
    /**
     * the omschrijving of an ministry
     *
     * @var string
    */
	protected $omschrijving ='';
	
    /**
     * the parentdepartment of an ministry
     *
     * @var string
    */
	protected $parentdepartment ='';
	
    /**
     * the soort of an ministry
     *
     * @var string
    */
	protected $soort ='';
	
    /**
     * the taakvisie of an ministry
     *
     * @var string
    */
	protected $taakvisie ='';
	
    /**
     * the toekomstverwachting of an ministry
     *
     * @var string
    */
	protected $toekomstverwachting ='';
	
    /**
     * the planactiviteiten of an ministry
     *
     * @var string
    */
	protected $planactiviteiten ='';
	
    /**
     * the volgendejaren of an ministry
     *
     * @var string
    */
	protected $volgendejaren ='';
	
    /**
     * Sets the uid
     *
     * @param int $uid
    */
    public function setUid($uid):void
    {
        $this->uid = $uid;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Ministry set uid : '.$this->uid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
    }

   /**
     * Returns the omschrijving
     *
     * @return string
    */
    public function getOmschrijving(): string
    {
        return $this->omschrijving;
    }
	/**
     * Sets the omschrijving
     *
     * @param string $omschrijving omschrijving
     * @return void
    */
    public function setOmschrijving($omschrijving): void
    {
        $this->omschrijving = $omschrijving;
    }

   /**
     * Returns the bedieningsleiders
     *
     * @return string|null
    */
    public function getBedieningsleiders(): ?string
    {
        return $this->bedieningsleiders;
    }
	/**
     * Sets the bedieningsleiders
     *
     * @param string $bedieningsleiders bedieningsleiders
    */
    public function setBedieningsleiders($bedieningsleiders): void
    {
        $this->bedieningsleiders = $bedieningsleiders;
    }

   /**
     * Returns the secretarissen
     *
     * @return string|null
    */
    public function getSecretarissen(): ?string
    {
        return $this->secretarissen;
    }
	/**
     * Sets the secretarissen
     *
     * @param string $secretarissen secretarissen
    */
    public function setSecretarissen($secretarissen): void
    {
        $this->secretarissen = $secretarissen;
    }

   /**
     * Returns the id_bedieningsleider
     *
     * @return string|null
    */
    public function getId_bedieningsleider(): ?string
    {
        return $this->id_bedieningsleider;
    }
	/**
     * Sets the id_bedieningsleider
     *
     * @param string $id_bedieningsleider id_bedieningsleider
    */
    public function setId_bedieningsleider($id_bedieningsleider): void
    {
        $this->id_bedieningsleider = $id_bedieningsleider;
    }

   /**
     * Returns the id_secretarissen
     *
     * @return string
    */
    public function getId_secretarissen(): string
    {
        return $this->id_secretarissen;
    }
	/**
     * Sets the id_secretarissen
     *
     * @param string $id_secretarissen id_secretarissen
    */
    public function setId_secretarissen($id_secretarissen): void
    {
        $this->id_secretarissen = $id_secretarissen;
    }

   /**
     * Returns the soort
     *
     * @return string
    */
    public function getSoort(): string
    {
        return $this->soort;
    }
	/**
     * Sets the soort
     *
     * @param string $soort soort
    */
    public function setSoort($soort): void
    {
        $this->soort = $soort;
    }

   /**
     * Returns the taakvisie
     *
     * @return string|null
    */
    public function getTaakvisie(): ?string
    {
        return $this->taakvisie;
    }
	/**
     * Sets the taakvisie
     *
     * @param string $taakvisie taakvisie
    */
    public function setTaakvisie($taakvisie): void
    {
        $this->taakvisie = $taakvisie;
    }

   /**
     * Returns the toekomstverwachting
     *
     * @return string|null
    */
    public function getToekomstverwachting(): ?string
    {
        return $this->toekomstverwachting;
    }
	/**
     * Sets the toekomstverwachting
     *
     * @param string $toekomstverwachting toekomstverwachting
    */
    public function setToekomstverwachting($toekomstverwachting): void
    {
        $this->toekomstverwachting = $toekomstverwachting;
    }

   /**
     * Returns the planactiviteiten
     *
     * @return string|null
    */
    public function getPlanactiviteiten(): ?string
    {
        return $this->planactiviteiten;
    }
	/**
     * Sets the planactiviteiten
     *
     * @param string $planactiviteiten planactiviteiten
    */
    public function setPlanactiviteiten($planactiviteiten): void
    {
        $this->planactiviteiten = $planactiviteiten;
    }

   /**
     * Returns the volgendejaren
     *
     * @return string|null
    */
    public function getVolgendejaren(): ?string
    {
        return $this->volgendejaren;
    }
	/**
     * Sets the volgendejaren
     *
     * @param string $volgendejaren volgendejaren
    */
    public function setVolgendejaren($volgendejaren): void
    {
        $this->volgendejaren = $volgendejaren;
    }

   /**
     * Returns the parentdepartment
     *
     * @return string|null
    */
    public function getParentdepartment(): ?string
    {
        return $this->parentdepartment;
    }
	/**
     * Sets the parentdepartment
     *
     * @param string $parentdepartment
    */
    public function setParentdepartment($parentdepartment): void
    {
        $this->parentdepartment = $parentdepartment;
    }


}
