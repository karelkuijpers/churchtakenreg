<?php
namespace Parousia\Churchtakenreg\Domain\Model;

/***
 *
 * This file is part of the "Churchtakenreg" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * 
 */
class Coworker extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * @var int
     */
    protected $uid = null;
	
    /**
     * @var int
     */
    protected $uid_parent = null;
	
	
    /**
     * @var int
     */
    protected $uidperson = null;

    /**
     * the name of the task this cowoker
     *
     * @var string
    */
	protected $parenttaskname ='';
	
    /**
     * the fullname of this coworker
     *
     * @var string
    */
	protected $fullname ='';
	
    /**
     * the start date of for this task
     *
     * @var /DateTime
    */
	protected $datestart ='';	
	
    /**
     * the end date for this task
     *
     * @var /DateTime
    */
	protected $dateend ='';	
	
    /**
     * remarks for this taak
     *
     * @var string
    */
	protected $remarks ='';
	
	
   /**
     * Returns the uid
     *
     * @return int $uid
     */
    public function getUid():?int
    {
        return (int)$this->uid;
    }

    /**
     * Sets the uid
     *
     * @param int $uid
     * @return void
    */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }
	
   /**
     * Returns the uidperson
     *
     * @return int $uidperson
     */
    public function getUidperson()
    {
        return $this->uidperson;
    }

    /**
     * Sets the uidperson
     *
     * @param int $uidperson
     * @return void
    */
    public function setUidperson($uidperson)
    {
        $this->uidperson = $uidperson;
    }

   /**
     * Returns the parenttaskname
     *
     * @return string $parenttaskname
    */
    public function getParenttaskname()
    {
        return $this->parenttaskname;
    }
	/**
     * Sets the parenttaskname
     *
     * @param string $parenttaskname
     * @return void
    */
    public function setParenttaskname($parenttaskname)
    {
        $this->parenttaskname = $parenttaskname;
    }

   /**
     * Returns the fullname
     *
     * @return string $fullname
    */
    public function getFullname()
    {
        return $this->fullname;
    }
	/**
     * Sets the fullname
     *
     * @param string $fullname
     * @return void
    */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

   /**
     * Returns the datestart
     *
     * @return /DateTime $datestart
    */
    public function getDatestart()
    {
        return $this->datestart;
    }
	/**
     * Sets the datestart
     *
     * @param /DateTime $datestart
     * @return void
    */
    public function setDatestart($datestart)
    {
        $this->datestart = $datestart;
    }

   /**
     * Returns the dateend
     *
     * @return /DateTime $dateend
    */
    public function getDateend()
    {
        return $this->dateend;
    }
	/**
     * Sets the dateend
     *
     * @param /DateTime $dateend
     * @return void
    */
    public function setDateend($dateend)
    {
        $this->dateend = $dateend;
    }

   /**
     * Returns the remarks
     *
     * @return string $remarks
    */
    public function getRemarks()
    {
        return $this->remarks;
    }
	/**
     * Sets the remarks
     *
     * @param string $remarks
     * @return void
    */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;
    }


}
