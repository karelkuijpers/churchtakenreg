<?php
namespace Parousia\Churchtakenreg\Domain\Model;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/***
 *
 * This file is part of the "Churchtakenreg" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * 
 */
class Task extends AbstractEntity
{
    /**
     * the gfts needed for this task
     *
     * @var string
    */
	protected $gaven ='';
	
    /**
     * the passion needed for this task
     *
     * @var string
    */
	protected $verlangen ='';
	
    /**
     * the style of organising needed for this task
     *
     * @var string
    */
	protected $organisatiestijl ='';	
	
    /**
     * the style of energy needed for this task
     *
     * @var string
    */
	protected $energiestijl ='';	
	
    /**
     * needed qualities for this taak
     *
     * @var string
    */
	protected $vaardigheden='';
	

    /**
     * the omschrijving of an taak
     *
     * @var string
    */
	protected $omschrijving ='';
	
    /**
     * the parentdepartment of this taak
     *
     * @var string
    */
	protected $parentdepartment ='';
	
    /**
     * the soort of parent of this taak
     *
     * @var string
    */
	protected $soort ='';
	
    /**
     * the obligations of this taak
     *
     * @var string
    */
	protected $verplichtingen ='';
	
    /**
     * the spiritual maturity of an taak
     *
     * @var string
    */
	protected $geestelijkerijpheid ='';
	
    /**
     * memmbership required for this taak
     *
     * @var string
    */
	protected $lidmaatschap ='';
	
    /**
     * expected working time for this taak
     *
     * @var string
    */
	protected $tijdsbeslag ='';
	
    /**
     * remarks for this taak
     *
     * @var string
    */
	protected $opmerkingen ='';
	
    /**
     * the quantity of cowokers needed for thsi taak
     *
     * @var int
    */
	protected $aantal =0;
	
    /**
     * the minimal assigment in years required for thsi taak
     *
     * @var int
    */
	protected $duur =0;
	
	
   /**
     * Returns the uid
     *
     * @return int $uid
     */
    public function getUid():?int
    {
        return (int)$this->uid;
    }

    /**
     * Sets the uid
     *
     * @param int $uid
    */
    public function setUid($uid): void
    {
        $this->uid = $uid;
    }

   /**
     * Returns the omschrijving
     *
     * @return string $omschrijving
    */
    public function getOmschrijving(): string
    {
        return $this->omschrijving;
    }
	/**
     * Sets the omschrijving
     *
     * @param string $omschrijving
    */
    public function setOmschrijving($omschrijving): void
    {
        $this->omschrijving = $omschrijving;
    }

   /**
     * Returns the gaven
     *
     * @return string|null
    */
    public function getGaven(): ?string
    {
        return $this->gaven;
    }
	/**
     * Sets the gaven
     *
     * @param string $gaven
    */
    public function setGaven($gaven): void
    {
        $this->gaven = $gaven;
    }

   /**
     * Returns the verlangen
     *
     * @return string|null
    */
    public function getVerlangen(): ?string
    {
        return $this->verlangen;
    }
	/**
     * Sets the verlangen
     *
     * @param string $verlangen
    */
    public function setVerlangen($verlangen): void
    {
        $this->verlangen = $verlangen;
    }

   /**
     * Returns the organisatiestijl
     *
     * @return string|null
    */
    public function getOrganisatiestijl(): ?string
    {
        return $this->organisatiestijl;
    }
	/**
     * Sets the organisatiestijl
     *
     * @param string $organisatiestijl
    */
    public function setOrganisatiestijl($organisatiestijl): void
    {
        $this->organisatiestijl = $organisatiestijl;
    }

   /**
     * Returns the energiestijl
     *
     * @return string|null
    */
    public function getEnergiestijl(): ?string
    {
        return $this->energiestijl;
    }
	/**
     * Sets the energiestijl
     *
     * @param string $energiestijl
    */
    public function setEnergiestijl($energiestijl): void
    {
        $this->energiestijl = $energiestijl;
    }

   /**
     * Returns the vaardigheden
     *
     * @return string|null
    */
    public function getVaardigheden(): ?string
    {
        return $this->vaardigheden;
    }
	/**
     * Sets the vaardigheden
     *
     * @param string
    */
    public function setVaardigheden($vaardigheden): void
    {
        $this->vaardigheden = $vaardigheden;
    }

   /**
     * Returns the soort
     *
     * @return string $soort
    */
    public function getSoort(): string
    {
        return $this->soort;
    }
	/**
     * Sets the soort
     *
     * @param string $soort
    */
    public function setSoort($soort): void
    {
        $this->soort = $soort;
    }

   /**
     * Returns the verplichtingen
     *
     * @return string|null
    */
    public function getVerplichtingen(): ?string
    {
        return $this->verplichtingen;
    }
	/**
     * Sets the verplichtingen
     *
     * @param string $verplichtingen
    */
    public function setVerplichtingen($verplichtingen): void
    {
        $this->verplichtingen = $verplichtingen;
    }

   /**
     * Returns the geestelijkerijpheid
     *
     * @return string|null
    */
    public function getGeestelijkerijpheid(): ?string
    {
        return $this->geestelijkerijpheid;
    }
	/**
     * Sets the geestelijkerijpheid
     *
     * @param string $geestelijkerijpheid
    */
    public function setGeestelijkerijpheid($geestelijkerijpheid): void
    {
        $this->geestelijkerijpheid = $geestelijkerijpheid;
    }

   /**
     * Returns the lidmaatschap
     *
     * @return string|null
    */
    public function getLidmaatschap(): ?string
    {
        return $this->lidmaatschap;
    }
	/**
     * Sets the lidmaatschap
     *
     * @param string $lidmaatschap
    */
    public function setLidmaatschap($lidmaatschap): void
    {
        $this->lidmaatschap = $lidmaatschap;
    }

   /**
     * Returns the tijdsbeslag
     *
     * @return string|null
    */
    public function getTijdsbeslag(): ?string
    {
        return $this->tijdsbeslag;
    }
	/**
     * Sets the tijdsbeslag
     *
     * @param string $tijdsbeslag
    */
    public function setTijdsbeslag($tijdsbeslag): void
    {
        $this->tijdsbeslag = $tijdsbeslag;
    }

   /**
     * Returns the remarks
     *
     * @return string|null
    */
    public function getOpmerkingen(): ?string
    {
        return $this->opmerkingen;
    }
	/**
     * Sets the remarks
     *
     * @param string $opmerkingen
    */
    public function setOpmerkingen($opmerkingen): void
    {
        $this->opmerkingen = $opmerkingen;
    }
	
   /**
     * Returns the aantal
     *
     * @return int|null
    */
    public function getAantal(): ?int
    {
        return $this->aantal;
    }
	/**
     * Sets the aantal
     *
     * @param int $aantal
    */
    public function setAantal($aantal): void
    {
        $this->aantal = intval($aantal);
    }

   /**
     * Returns the duur
     *
     * @return int|null
    */
    public function getDuur(): ?int
    {
        return $this->duur;
    }
	/**
     * Sets the duur
     *
     * @param int $duur
    */
    public function setDuur($duur): void
    {
        $this->duur = intval($duur);
    }

   /**
     * Returns the parentdepartment
     *
     * @return int|nullt
    */
    public function getParentdepartment(): ?string
    {
        return $this->parentdepartment;
    }
	/**
     * Sets the parentdepartment
     *
     * @param string $parentdepartment
    */
    public function setParentdepartment($parentdepartment): void
    {
        $this->parentdepartment = $parentdepartment;
    }


}
