<?php

namespace Parousia\Churchtakenreg\Controller;
use Parousia\Churchtakenreg\Domain\Model\Ministry;
//use Parousia\Churchtakenreg\Domain\Model\Task;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Psr\Http\Message\ResponseInterface;


/***************************************************************
*  Copyright notice
*
*  (c) 2019 T.Duinker <tobby.duinker@gmail.com>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * MinistryController
 *
 */
class MinistryController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

/**
     * @var \Parousia\Churchtakenreg\Domain\Repository\MinistryRepository
     */
    protected $MinistryRepository;
	var $frontendController;
	var $conf;
	var $frontendUser;
	var $userid=0;
	var $username='';

	//var $aParms=array();
	//var $db;
	
    /**
     * Inject a ministry repository to enable DI
     *
     * @param \Parousia\Churchceanteamsreg\Domain\Repository\MinistryRepository $ministryRepository
     */
    public function injectMinistryRepository(\Parousia\Churchtakenreg\Domain\Repository\MinistryRepository $ministryRepository)
    {
        $this->MinistryRepository = $ministryRepository;
    }
	    /**
     * Initialize 
     */
    public function initializeAction(): void
    {
		churchpersreg_div::connectdb($this->db);
		$this->frontendController = $this->request->getAttribute('frontend.controller');
		$this->conf=array();
		$this->conf['lang'] = $this->frontendController->config['config']['language'];
		$this->frontendUser = $this->request->getAttribute('frontend.user');
		$this->userid=$this->frontendUser->user['person_id'];
		$this->username=$this->frontendUser->user['name'];
		$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchtakenreg');
		$this->conf['pagemailen']= $extensionConfiguration['pagemailen'];	
		$pageArguments = $this->request->getAttribute('routing');
		$this->conf['pageId']= $pageArguments->getPageId();	

	}
  /**
   * action showhierarchy
   * 
   * @return void
   */

  public Function showhierarchyAction(): ResponseInterface  {

		$select='';
		$stack=array();
		$return='';
		$retextensionName='';
		$retpluginName='';
		$retpageUid='';
		$retcontroller='';
		$args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'showhierarchyAction args: '.urldecode(http_build_query($args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		if (!empty($args) && is_array($args)){
			if (!empty($args['ministryid']))$select=trim($args['ministryid']);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'showhierarchyAction select: '.$select."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			if (!empty($args['stack']))$stack=json_decode($args['stack'],true,20,JSON_INVALID_UTF8_IGNORE);
			$ptr=count($stack)-1;
			if (!empty($stack[$ptr]['action']))$return=$stack[$ptr]["action"];
			if ($return == 'showhierarchy') $return='';  // niet terug naar zichzelf
			else
			{
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction return: '.$return.'; stack['.$ptr.']: '.urldecode(http_build_query($stack[$ptr],NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
				if (!empty($stack[$ptr]["extensionName"]))$retextensionName=$stack[$ptr]["extensionName"];
				if (!empty($stack[$ptr]["pluginName"]))$retpluginName=$stack[$ptr]["pluginName"];
				if (!empty($stack[$ptr]["pageUid"]))$retpageUid=$stack[$ptr]["pageUid"];
				if (!empty($stack[$ptr]["controller"]))$retcontroller=$stack[$ptr]["controller"];
				$stack[]=array("action"=>"showhierarchy");
		}
		}
		//$select="3476_m_drop";
		$row=array();

        $assignedValues = [
        'ministry' => $row,
		'myconfig' => urlencode(json_encode($this->conf)),
		'select' => $select,
        ];
		if (!empty($return))
		{
				$assignedValues['return']=$return;	
				$assignedValues['retcontroller']= $retcontroller;
				$assignedValues['retextensionName']= $retextensionName;
				$assignedValues['retpluginName']= $retpluginName;
				$assignedValues['retpageUid']= $retpageUid;
				$assignedValues['stack'] = json_encode($stack);
		}
		
        $this->view->assignMultiple($assignedValues);   

		$extPath = PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('churchtakenreg'));

		$assetCollector = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\AssetCollector::class);
		$assetCollector->addStyleSheet('contextmenu','EXT:churchtakenreg/Resources/Public/Css/contextMenu.css',['media' => 'all','type'=> 'text/css'],['priority' => true]);
		$assetCollector->addStyleSheet('ministry','EXT:churchtakenreg/Resources/Public/Css/ministry.css',['media' => 'all','type'=> 'text/css'],['priority' => true]);
		$assetCollector->addJavaScript('contextMenujs', 'EXT:churchtakenreg/Resources/Public/Javascript/jquery.contextMenu.js',[], ['priority' => true]);
		$assetCollector->addJavaScript('bedieningsregistratie', 'EXT:churchtakenreg/Resources/Public/Javascript/bedieningsregistratie.js',[], ['priority' => true]);
			
		$assetCollector->addInlineJavaScript('churchtakenregjs',
		'	$ = jQuery.noConflict();
			/*<![CDATA[*/

		var myconfig="'.urlencode(json_encode($this->conf)).'";
		var personaction ="index.php?eID=selectperson";
		var ministryaction = "index.php?eID=tx_churchtakenreg_eID";
		var username = "'.$this->username.'";
		var role="";


		function tx_ministry_submit(value, action,eigen) {
			var mode = action;
			//alert("action:"+action+"; eigen:"+eigen);
			$.ajax({
				method: "POST",
				url: "index.php?eID=tx_churchtakenreg_eID",
				data: {
                   "ref": value,
					"config": myconfig,
                    "action": action,
					"event": value,
                    "eigen": eigen,
				},
				success: function(result,status,xhr){
					if (mode=="editMinistry"||mode=="editTask"||mode=="editWorker") {
						$("#ministryeditview").html(result);
						$("#ministryeditview").show();
						$("#errorBox").hide();
                        $("html,body").animate({
                            scrollTop: $("#ministryeditview").offset().top - 80
                        });
					} else 
					{
						$("#ministryeditview").hide();
						$("#ministryeIDResult").html(result);
					}
				}
			});
		}

		function tx_ministry_editsubmit(value, action) {
			var mode = action;
			var id = value;
			if (mode=="cancelMinistry"){
				$("#ministryeditview").hide();
				return;
			}
			if (mode =="deleteMinistry")
			{
		    	// check it has no children:
				myclass=$("#"+value+"_toggle").attr("class");
				//alert("action:"+action+", class:"+myclass);
				
				if (myclass!="eind") {
					$("#errorBoxMinistry").show();
					$("#errorBoxMinistry").text("Er zijn nog bedieningen/taken onder deze bediening");
					return;
				}
			}
			if (mode =="saveMinistry" || mode =="savenewMinistry")
			{
		    	// check data:
				if ($("#ministrydescription").val()=="")
				{
					$("#errorBoxMinistry").show();
					$("#errorBoxMinistry").text("Omschrijving mag niet leeg zijn");
					return;					
				}
			}
			$.ajax({
				method: "POST",
				url: "index.php?eID=tx_churchtakenreg_eID",
				data: {
                   "ref": value,
					"config": myconfig,
                    "action": action,
					"event": value,
					"omschrijving": $("#ministrydescription").val(),
					"id_bedieningsleider": $("#idbedieningsleider").val(),
					"secretariaat": $("#idsecretariaat").val(),
					"soort": $("input[type=radio][name=soort]:checked").val(),
					"taakvisie": $("#taakvisie").val(),
					"toekomstverwachting": $("#toekomst").val(),
					"planactiviteiten": $("#planactiviteiten").val(),
					"volgendejaren": $("#volgendejaren").val(),
				},
				success: function(result,status,xhr){
					if (mode=="saveMinistry") {
						$("#ministryeditview").hide();
						//alert("result:"+result.toString()+"; lengte:"+result.length);
						var data = $.parseJSON(result);
						//alert("soort:"+data[\'soort\']+"; omschrijving:"+data[\'omschrijving\']);
						soort=data.soort;
						$("#"+id+"_drop").text(soort+":"+data.omschrijving);
						classname=$("#"+id+"_drag").attr("class");
						if (classname.search("open")==-1) open="closed"; else open="open";
						bediening=classname.search("blue");
						if ((bediening==-1 && soort=="bediening") || (bediening>-1 && soort=="afdeling"))
						{	// something changed:
							if (soort=="afdeling") $("#"+id+"_drag").attr("class",open); else $("#"+id+"_drag").attr("class","blue"+open);
						}
					}
					else if (mode=="savenewMinistry") {
					
						if (result.substring(0,6)=="error:")
						{
							$("#errorBoxMinistry").show();
							$("#errorBoxMinistry").text(result.substring(7));
							return;	
						}
						$("#ministryeditview").hide();
						$("#"+id+"_li ul").remove();
						//alert("voeg toe aan:"+id+"_li");
						$("#"+id+"_li").append(result); // display new sub tree, append to bediening
						toggle(id,true);
					}
					 else if (mode=="addMinistry")
					{
						$("#ministryeditview").html(result);
						$("#ministryeditview").show();
						$("#errorBoxMinistry").hide();
					}
					else if (mode=="deleteMinistry")
					{
						$("#ministryeditview").hide();
						var par=$("#"+id+"_li").parent()
						var parid=par.attr("id");  
						$("#"+id+"_li").remove();
						//alert ("parid:"+parid+"; nr children:"+par.children("li").length);
						if (par.children("li").length==0) toggle(parid);
					}
				}
			});
		}


		function tx_task_editsubmit(value, action) {
			var mode = action;
			var id = value;
			var energiestijl="";
			var stijlorganisatie="";
			//alert ("action:"+action+";id:"+id);
			if (mode=="cancelTask"){
				$("#ministryeditview").hide();
				return;
			}
			if (mode =="deleteTask")
			{
		    	// check it has no children:
				myclass=$("#"+value+"_toggle").attr("class");
				//alert("action:"+action+", class:"+myclass);
				
				if (myclass!="eind") {
					$("#errorBoxtask").show();
					$("#errorBoxtask").text("Er zijn nog medewerkers onder deze taak");
					return;
				}
			}
			if (mode =="saveTask" || mode =="savenewTask")
			{
		    	// check data:
				if ($("#taskdescription").val()=="")
				{
					$("#errorBoxTask").show();
					$("#errorBoxTask").text("Omschrijving mag niet leeg zijn");
					return;					
				}
				var gaven=$("#gift1").val()+","+$("#gift2").val()+","+$("#gift3").val();
				gaven=gaven.replace(",,",",");
				if ($("input[name=\'stijlenergie\']").is(\':checked\'))energiestijl=$("input[name=\'stijlenergie\']:checked").val();
				if ($("input[name=\'stijlorganisatie\']").is(\':checked\'))stijlorganisatie=$("input[name=\'stijlorganisatie\']:checked").val();
			}
			$.ajax({
				method: "POST",
				url: "index.php?eID=tx_churchtakenreg_eID",
				data: {
                   "ref": value,
					"config": myconfig,
                    "action": action,
					"event": value,
					"omschrijving": $("#taskdescription").val(),
					"verlangen": $("#passion").val(),
					"gaven": gaven,
					"energiestijl": energiestijl,
					"organisatiestijl": stijlorganisatie,
					"vaardigheden": $("#vaardigheden").val(),
					"verplichtingen": $("#verplichtingen").val(),
					"geestelijke_rijpheid": $("#geestelijke_rijpheid").val(),
					"lidmaatschap":($("#lidmaatschap").is(":checked")?"is lid":"geen lid"),
					"aantal": $("#aantal").val(),
					"duur": $("#duur").val(),
					"tijdsbeslag": $("#tijdsbeslag").val(),
					"opmerkingen": $("#opmerkingen").val(),
				},
				success: function(result,status,xhr){
					if (mode=="saveTask") {
						$("#ministryeditview").hide();
						//alert("result:"+result.toString()+"; lengte:"+result.length);
						var data = $.parseJSON(result);
						//alert("soort:"+data[\'soort\']+"; omschrijving:"+data[\'omschrijving\']);
						$("#"+id+"_drop").text("taak: "+data.omschrijving);
					}
					else if (mode=="savenewTask") {
						if (result.substring(0,6)=="error:")
						{
							$("#errorBoxtask").show();
							message=result.substring(7);
							$("#errorBoxtask").text(message);
							return;	
						}
						$("#ministryeditview").hide();
						$("#"+id+"_li ul").remove();
						//alert("voeg toe aan:"+id+"_li");
						$("#"+id+"_li").append(result); // display new sub tree, append to bediening
						toggle(id,true);

					}
					 else if (mode=="addTask")
					{
						$("#ministryeditview").html(result);
						$("#ministryeditview").show();
						$("#errorBoxMinistry").hide();
					}
					else if (mode=="deleteTask")
					{
						$("#ministryeditview").hide();
						var par=$("#"+id+"_li").parent()
						var parid=par.attr("id");  
						$("#"+id+"_li").remove();
						if (par.children("li").length==0)toggle(parid); 
					}
				}
			});
		}


		function tx_worker_editsubmit(value, action) {
			var mode = action;
			var id = value;
   			//alert ("action:"+action+";id:"+id);
			if (mode=="cancelWorker"){
				$("#ministryeditview").hide();
				return;
			}
			if (mode =="deleteWorker")
			{
				if (confirm("Weet u zeker dat u deze taak voor "+$("#persoonworker").val()+" wilt beëindigen?")==false) return;
			}
			if (mode =="saveWorker" || mode =="savenewWorker")
			{
		    	// check data:
				if ($("#idworker").val()=="" || $("#persoonworker").val()=="")
				{
					$("#errorBoxworker").show();
					$("#errorBoxworker").text("Vul een medewerker in");
					return;					
				}
				//alert("opmerkingen:"+$("#opmerkingen").val());
			}
			$.ajax({
				method: "POST",
				url: "index.php?eID=tx_churchtakenreg_eID",
				data: {
                   "ref": value,
					"config": myconfig,
                    "action": action,
					"event": value,
					"id_persoon": $("#idworker").val(),
					"datum_start": $("#startdate").val(),
					"opmerkingen": $("#opmerkingen").val(),
				},
				success: function(result,status,xhr){
                    if (mode=="savenewWorker"||mode=="saveWorker") {
                        //alert("start:"+$("#startdate").val()+"; result :"+result.substring(0,6));
						if (result.substring(0,6)=="error:")
						{
							$("#errorBoxworker").show();
							$("#errorBoxworker").text(result.substring(7));
							return;	
						}
                    }
					if (mode=="saveWorker") {
                        //alert("persoon :"+$("#persoonworker").val());
						$("#"+id+"_drop").text("medewerker: "+$("#persoonworker").val());
						$("#ministryeditview").hide();
					}
					else if (mode=="savenewWorker") {
						$("#ministryeditview").hide();
						$("#"+id+"_li").parent().replaceWith(result);  //replace parent ul with tasks and employees
						//alert("voeg toe aan:"+id+"_li");
						//$("#"+id+"_li").append(result); // display new sub tree, append to taak
						$("#"+id+"_toggle").attr("class","plus");
					}
					 else if (mode=="addWorker")
					{
						$("#ministryeditview").html(result);
						$("#ministryeditview").show();
						$("#errorBoxworker").hide();
					}
					else if (mode=="deleteWorker")
					{
						$("#ministryeditview").hide();
						id=id+"_li";
						//alert("verwijder id:"+id);
						var par=$("#"+id).parent()
						//alert ("id van parent:"+par.attr("id"));
						var parid=par.attr("id")+"_toggle";  
						$("#"+id).remove();
						//alert ("parid:"+parid+"; nr children:"+par.children("li").length);
						if (par.children("li").length==0) $("#"+parid).attr("class","eind");
					}
				}
			});
		}

		$.fn.center = function () {
		    this.css("position","fixed");
		    this.css("top", (($(window).height() - this.outerHeight()) / 2) /*+ $(window).scrollTop()*/ + "px");
		    this.css("left", (($(window).width() - this.outerWidth()) / 2) /*+ $(window).scrollLeft()*/ + "px");
		    return this;
		}
		
		function download(filename) {
			//alert ("filenaam:"+filename);
			window.open(filename,"_parent");
		}
		
		
$(function() {
     $.contextMenu({
        selector: ".ctxtbd",
		trigger: "hover", 
		/*delay: 500,*/
		autoHide: true,
		reposition: true,
		zIndex: 0,
        callback: function(key, options) {
			if (key=="wijzig")
			{
				$(this).trigger("click");
				return;
			}
			id=$(this).attr("id").split("_")[0];
			eigen=$(this).attr("eigen");
		             //var m = "clicked: " + key+" id:"+id+" tekst:"+$(this).text();
		             //window.console && console.log(m) || alert(m);
			$.ajax({
				method: "POST",
				url: ministryaction,
				data: {
					"config": myconfig,
                    "action": "exportBediening",
					"event": id,
                    "eigen": eigen,
					"soort": key,
					"username": username,
				},
				dataType: "json",
				success: function(result,status,xhr){
					if (result.status=="success")
					{
						//alert("succesvol gedownload:"+result.exportnaam);
						download(result.exportnaam);
					}
					else alert("niets gevonden");
				},
				error: function (result, statusText, jqXHR) {
		           	alert("Error: ("+statusText+")" + jqXHR );
				},
			});
         },
         items: {
			"wijzig": {name: "Open afdeling/bediening"},
			"sep1": "---------",
		             "pertaak": {name: "Export medewerkers per taak/bediening"},
		             "alle": {name: "Export alle geslecteerde medewerkers"},
		            	"ldr": {name: "Export alle bijbehorende bedieningsleiders"}
         }
     });
     $.contextMenu({
        selector: ".ctxttk", 
		trigger: "hover", 
		/*delay: 500,*/
		autoHide: true,
		zIndex: 0,
        callback: function(key, options) {
			if (key=="wijzig")
			{
				$(this).trigger("click");
				return;
			}
			id=$(this).attr("id").split("_")[0];
			eigen=$(this).attr("eigen");
			$.ajax({
				method: "POST",
				url: ministryaction,
				data: {
					"config": myconfig,
                    "action": "exportBediening",
					"event": id,
                    "eigen": eigen,
					"soort": key,
					"username": username,
				},
				dataType: "json",
				success: function(result,status,xhr){
					//alert("succesvol gedownload:"+result.exportnaam);
					download(result.exportnaam);
				},
				error: function (result, statusText, jqXHR) {
		           	alert("Error: ("+statusText+")" + jqXHR );
				},
			});
         },
         items: {
			"wijzig": {name: "Open taak"},
			"sep1": "---------",
		             "taak": {name: "Export alle geslecteerde medewerkers"}
         }
     });
 });
	',[],['priority' => true]);
	
	/*<script>
	  $( function() {
	    $( ".draggable" ).draggable();
	  } );
	</script>
	'); */
	return $this->htmlResponse();
	}
	

}

