<?php
namespace Parousia\Churchtakenreg\Hooks;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;
use Parousia\Churchpersreg\Hooks\FluidTemplate;
use Parousia\Churchgiftsreg\Hooks\churchgiftsreg_div;
use Parousia\Churchtakenreg\Domain\Model\Ministry;
use Parousia\Churchtakenreg\Domain\Model\Task;
use Parousia\Churchtakenreg\Domain\Model\Coworker;
use Parousia\Churchtakenreg\Domain\Model\Tasktree;
use Parousia\Churchtakenreg\Domain\Model\Workertree;
use Parousia\Churchtakenreg\Domain\Model\Ministrytree;
use TYPO3\CMS\Fluid\View\StandaloneView;


ini_set("display_errors",1);
ini_set("log_errors",1);
mb_internal_encoding("UTF-8");
/***************************************************************
*  copyright notice
*
*  (c) 2009-2013 joachim ruhs (postmaster@joachim-ruhs.de)
*  all rights reserved
*
*  this script is part of the typo3 project. the typo3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the gnu general public license as published by
*  the free software foundation; either version 2 of the license, or
*  (at your option) any later version.
*
*  the gnu general public license can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*  a copy is found in the textfile gpl.txt and important notices to the license
*  from the author is found in license.txt distributed with these scripts.
*
*
*  this script is distributed in the hope that it will be useful,
*  but without any warranty; without even the implied warranty of
*  merchantability or fitness for a particular purpose.  see the
*  gnu general public license for more details.
*
*  this copyright notice must appear in all copies of the script!
***************************************************************/
/**
* class.tx_churchtakenreg_eid.php
*
*
* @author karel kuijpers <karelkuijpers@gmail.com>
*/


/**
 *
 *
 * @author Karel Kuijpers <karelkuijpers@gmail.com>
 * @package TYPO3
 * @subpackage tx_churchtakenreg
 */

class tx_churchtakenreg_eID {
	protected $conf= array();
	protected $eersteafd='';
	protected $eerstetaak='';
	protected $eerstemed='';
	protected $a_rijpheid=array(" ",'zoekend', 'groeiend', 'stabiel', 'leider');
	protected $ErrMsg='';
	protected $db;
	protected $aExport;
	protected $soort;
	protected $permissie;
	protected $feUserUid;
	protected $personid;
	protected $bedieningEdit;
	protected $exportpersons;
	protected $cleanteamsbeheer;
	protected $antwoord ='';
	protected $aParms;
	protected $ministries;
	protected $ministrytree;
	public $request;

	/**
 	* @var \TYPO3\CMS\Fluid\View\StandaloneView
 	*/
	protected $view;

/*	public function __construct(\TYPO3\CMS\Fluid\View\StandaloneView $StandaloneView) {
	    $this->view = $StandaloneView;
	    $this->view->setFormat('html');
	} */


/**
 * @param ServerRequestInterface $request
 * @return ResponseInterface
 */
	public function processRequest(ServerRequestInterface $request):ResponseInterface
	{
		session_start();
		$action='';
		$response = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Http\Response::class);
		if (!empty($_SESSION["permissie"])){
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'churchtakenreg session permissie : '.http_build_query($_SESSION,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			$this->personid = $_SESSION['userid'];
			$this->feUserUid = $_SESSION['feuserid'];
			$this->permissie=$_SESSION["permissie"];
			if (!$this->Heeftpermissie("bedieningstructuur lezen")) die ("You are not privileged to perform this action");
			if (!empty($this->feUserUid) and !empty($this->personid))
			{
				$this->bedieningEdit = ($this->Heeftpermissie("bedieningstructuur schrijven"))?'1':'0';
				$this->exportpersons = ($this->Heeftpermissie("export personen"))?'1':'0';
				$this->cleanteamsbeheer = ($this->Heeftpermissie("Cleanteams samenstellen"))?'1':'0';
			}
		}
		else
		{
			$response->getBody()->write("Uw sessie is verlopen. Vernieuw het scherm");
			return $response;
		}

//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'view ministry params : '.http_build_query($request->getQueryParams(),'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

		// reading data vars for $this->conf
		$this->request =$request;
		$this->aParms= $request->getParsedBody();
		if (isset($this->aParms))
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'view ministry/task params : '.urldecode(http_build_query($this->aParms,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			if (isset($this->aParms['action']))$action=$this->aParms['action'];else $action='';
			if (isset($this->aParms['config'])) $config = urldecode($this->aParms['config']);
			$this->conf = json_decode($config,true);
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Globals json config: '.$config."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		}
		else
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'view ministry/task no params'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		}


		//if (is_array($this->conf))error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'conf definitief: '.http_build_query($this->conf,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		churchpersreg_div::connectdb($this->db);

		if (isset($this->conf['lang']))setlocale(LC_TIME,$this->conf['lang'].'_'.strtoupper ($this->conf['lang']) );
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'view ministry action : '.$action."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

		$this->antwoord .='';
		switch ($action) { 
			case 'viewMinistryTree': $this->viewMinistryTree();
			break;
			case 'editMinistry': $this->viewEditMinistry($this->aParms['event'],$this->aParms['eigen']);
			break;
			case 'saveMinistry': $this->saveMinistry($this->aParms['event']);
			break;
			case 'cancelMinistry': $this->cancelMinistry();
			break;
			case 'deleteMinistry': $this->deleteMinistry($this->aParms['event']);
			break;
			case 'addMinistry': $this->viewaddMinistry($this->aParms['event']);
			break;
			case 'savenewMinistry': $this->savenewMinistry($this->aParms['event']);
			break;
			case 'editTask': $this->viewTask($this->aParms['event'],$this->aParms['eigen']);
			break;
			case 'saveTask': $this->saveTask($this->aParms['event']);
			break;
			case 'cancelTask': $this->cancelTask();
			break;
			case 'deleteTask': $this->deleteTask($this->aParms['event']);
			break;
			case 'addTask': $this->viewaddTask($this->aParms['event']);
			break;
			case 'savenewTask': $this->savenewTask($this->aParms['event']);
			break;
			case 'editWorker': $this->viewWorker($this->aParms['event'],$this->aParms['eigen']);
			break;
			case 'saveWorker': $this->saveWorker($this->aParms['event']);
			break;
			case 'cancelWorker': $this->cancelWorker();
			break;
			case 'deleteWorker': $this->deleteWorker($this->aParms['event']);
			break;
			case 'addWorker': $this->viewaddWorker($this->aParms['event']);
			break;
			case 'savenewWorker': $this->savenewWorker($this->aParms['event']);
			break;
			case 'exportBediening': $this->exportBediening($this->aParms['event'],$this->aParms['soort'],$this->aParms['username']);
			break;
			case 'droptaak': $this->dropTaak($this->aParms['id'],$this->aParms['destId'],$this->aParms['soort']);
			break;

		    default: $this->antwoord .="Bad mode!";
		}
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'processRequest antwoord terug : '."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

			$response->getBody()->write($this->antwoord);
			return $response;

	}
	/**
		* Returns Ministry
		* @param int $id
       	* @return Ministry
	*/
	function getMinistrySingle($id): Ministry
	{
	    //error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getMinistrySingle id : '.$id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$query="select bp.*,group_concat(concat(s.roepnaam,' ',s.tussenvoegsel,'',s.achternaam) separator ',') as secretarissen ".
		" from (SELECT bed.*,group_concat(concat(p.roepnaam,' ',p.tussenvoegsel,' ',p.achternaam) separator ',') as bedieningsleiders FROM (select a.*,pa.omschrijving as onderafdeling from bediening as a ".
		"left join bediening as pa on (a.id_parent=pa.uid) where a.uid='".$id."') as bed left join persoon p on(find_in_set(p.uid,bed.id_bedieningsleider) and p.deleted=0)) as bp left join persoon s on (find_in_set(s.uid,bp.secretariaat) and s.deleted=0) limit 1";
	    //error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'view ministry query : '.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$result=$this->db->query($query);
		$row=$result->fetch_array(MYSQLI_ASSOC);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getMinistrySingle error : '.$this->db->error.'; result: '.urldecode(http_build_query($row,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$ministry=new Ministry();
		$ministry->setUid($row['uid']);
		$ministry->setOmschrijving($row['omschrijving']);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getMinistrySingle omschrijving : '.$ministry->getOmschrijving()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$ministry->setParentdepartment($row['onderafdeling']);
		$ministry->setBedieningsleiders($row['bedieningsleiders']);
		$ministry->setSecretarissen($row['secretarissen']);
		$ministry->setId_bedieningsleider($row['id_bedieningsleider']);
		$ministry->setId_secretarissen($row['secretariaat']);
		$ministry->setSoort($row['soort']);
		$ministry->setTaakvisie($row['taakvisie']);
		$ministry->setToekomstverwachting($row['toekomstverwachting']);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getMinistrySingle toekomstverwachting : '.$ministry->getToekomstverwachting()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$ministry->setPlanactiviteiten($row['planactiviteiten']);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getMinistrySingle Planactiviteiten : '.$ministry->getPlanactiviteiten()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$ministry->setVolgendejaren($row['volgendejaren']);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getMinistrySingle Volgendejaren : '.$ministry->getVolgendejaren()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		return $ministry;
	}

	/**
		* Returns Task
		* @param int $id
       	* @return \Parousia\Churchtakenreg\Domain\Model\Task
	*/
	function getTaskSingle($id)
	{
	    //error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getTaskSingle id : '.$id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$query="select ta.*,pa.omschrijving as parentname, pa.soort from taak ta,bediening pa where ta.uid=".$id." and ta.id_parent=pa.uid";
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getTaskSingle query : '.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$result=$this->db->query($query);
		$row=$result->fetch_array(MYSQLI_ASSOC);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getTaskSingle error : '.$this->db->error.'; result: '.urldecode(http_build_query($row,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

		$task=new Task();
		$task->setUid($row['uid']);
		$task->setOmschrijving($row['omschrijving']);
		$task->setParentdepartment($row['parentname']);
		$task->setSoort($row['soort']);
		$task->setVerlangen($row['verlangen']);
		$task->setGaven($row['gaven']);
		$task->setVaardigheden($row['vaardigheden']);
		$task->setOrganisatiestijl($row['organisatiestijl']);
		$task->setEnergiestijl($row['energiestijl']);
		$task->setGeestelijkerijpheid($row['geestelijke_rijpheid']);
		$task->setLidmaatschap($row['lidmaatschap']);
		$task->setDuur($row['duur']);
		$task->setTijdsbeslag($row['tijdsbeslag']);
		$task->setVerplichtingen($row['verplichtingen']);
		$task->setAantal($row['aantal']);
		$task->setOpmerkingen($row['opmerkingen']);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getTaskSingle task Geestelijkerijpheid : '.$task->getGeestelijkerijpheid()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getTaskSingle task Energiestijl : '.$task->getEnergiestijl()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getTaskSingle task vaardigheden : '.$task->getVaardigheden()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getTaskSingle task Opmerkingen : '.$task->getOpmerkingen()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		Return $task;
	}
	
	/**
		* Returns Coworker
		* @param int $id
       	* @return \Parousia\Churchtakenreg\Domain\Model\Coworker
	*/
	function getCoworkerSingle($id)
	{
		$fields = 'tb.datum_start,tb.id_persoon,tb.id_parent,AES_DECRYPT(tb.opmerkingen,@password) as opmerkingen,ta.omschrijving as parentname,trim(concat(roepnaam,if(tussenvoegsel<>"",concat(" ",tussenvoegsel),"")," ",achternaam)) as persoonsnaam';
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getCoworkerSingle fields:'.$fields."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$tables="taak ta,taakbekleding tb,persoon as p";
		$where='tb.uid="'.$id.'" and tb.id_parent=ta.uid and p.uid=tb.id_persoon';
		$query='select '.$fields.' from '.$tables.' where '.$where.' limit 1';
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewWorker query:'.$query ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$result=$this->db->query($query);
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewWorker error:'.$this->db->error.'; aantal:'.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		
		$row=$result->fetch_array(MYSQLI_ASSOC);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getTaskSingle error : '.$this->db->error.'; result: '.urldecode(http_build_query($row,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

		$startdate= new \DateTime($row['datum_start']);
		$coworker=new Coworker();
		$coworker->setUid($id);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getTaskSingle Uid : '.$coworker->getUid()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$coworker->setUidperson($row['id_persoon']);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getTaskSingle Uidperson : '.$coworker->getUidperson()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$coworker->setParenttaskname($row['parentname']);
		$coworker->setFullname($row['persoonsnaam']);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getTaskSingle Fullname : '.$coworker->getFullname()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$coworker->setRemarks($row['opmerkingen']);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getTaskSingle Reamrks : '.$coworker->getRemarks()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$coworker->setDatestart($startdate);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getTaskSingle startdate : '.$coworker->getDatestart()->format("Y-m-d")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		Return $coworker;
	}

  /**
     * Returns the Ministrytree
     *
     * //@return Parousia\Churchtakenreg\Domain\Model\Ministrytree
    */
	function GetTree($id_owner=0,$eigenbediening=false)
	{	
		// select all ministries with their tasks and workers
		$query= "
SELECT b.uid, b.omschrijving, soort,b.id_parent,(find_in_set('".$this->personid."',id_bedieningsleider) or find_in_set('".$this->personid."',secretariaat)) as eigen, GROUP_CONCAT(t.uid,'#$#',t.omschrijving,'#$#',t.workers separator '#%#') as taken 
FROM `bediening` b 
left join 
(
	select t.uid,t.omschrijving,t.id_parent,coalesce(GROUP_CONCAT(w.uid,'%$%',w.persoonsnaam separator '%#%'),'') as workers 
	from taak t 
	left join 
	(
		select w.uid,w.id_parent,trim(concat(roepnaam,if(tussenvoegsel<>'',concat(' ',tussenvoegsel),''),' ',achternaam)) as persoonsnaam 
		from taakbekleding w,persoon p where p.uid=w.id_persoon order by p.achternaam
	) as w on (t.uid=w.id_parent) group by t.uid 
) as t    
on (t.id_parent=b.uid) group by b.uid order by b.id_parent,b.omschrijving ";
/*		if (empty($id_owner)) $query	.= " b.id_parent='".$id_owner."'";
		else $query	.= " uid='".$id_owner."'"; */
		churchpersreg_div::connectdb($db);

		$result = $db->query($query)  or die("Can't perform Query: $query");	
		if (!$result || $result->num_rows==0) return null;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetMinistryTree query:'.$query."; error:".$db->error.'; num:'.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$this->ministries=$result->fetch_all(MYSQLI_ASSOC); 
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetMinistryTree ministries : '.urldecode(http_build_query($this->ministries,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

		
		$this->ministrytree= new Ministrytree();
		if ($id_owner==0) 
		{
			$this->ministrytree->setUid(0);
			$this->ministrytree->setOmschrijving('');
			$this->ministrytree->setSoort('afdeling');
			$this->ministrytree->setEigen($eigenbediening);
			$this->eersteafd='1_a';
		}
		else
		{
			// search id_owner:
			$idx= array_search($id_owner, array_column($this->ministries, 'uid'));
			$this->ministrytree->setUid($this->ministries[$idx]['uid']);
			$this->ministrytree->setOmschrijving($this->ministries[$idx]['omschrijving']);
			$this->ministrytree->setSoort($this->ministries[$idx]['soort']);
			$this->ministrytree->setEigen(($this->ministries[$idx]['eigen']||$eigenbediening));
			if (!empty($this->ministries[$idx]['taken']))$this->GetTasks($this->ministrytree,$this->ministries[$idx]['taken']);
		
		}
		
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetMinistryTree first uid:'.$this->ministries[0]['uid']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$this->GetSubMinistries($this->ministrytree);

		//$this->ministrytrees->attach($ministrytree);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetMinistryTree row:'.urldecode(http_build_query($ministries,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		
		
	}

	
   /**
     * Fill subministries of $owner
     *
	 * @param \Parousia\Churchtakenreg\Domain\Model\Ministrytree $owner
     * @return void
    */
	function GetSubMinistries($owner)
	{	
		$ownerid=$owner->getUid();
		$eigenbediening=$owner->getEigen();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetMinistryTree owner '.$owner->getOmschrijving().' : '.$sub['omschrijving']."; eigen:".$owner->getEigen()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$subs= array_filter(
			$this->ministries,
			function ($val) use ($ownerid){
				return ($val['id_parent']==$ownerid);
			});
		foreach ($subs as $sub)
		{
			$ministrytree= new Ministrytree();
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetMinistryTree sub '.urldecode(http_build_query($sub,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetMinistryTree new ministry onder '.$owner->getOmschrijving().' : '.$sub['omschrijving']."; eigen:".$sub['eigen']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

			$ministrytree->setUid($sub['uid']);
			$ministrytree->setOmschrijving($sub['omschrijving']);
			$ministrytree->setSoort($sub['soort']);
			$ministrytree->setEigen($sub['eigen'] ||$eigenbediening);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetMinistryTree new ministry onder '.$owner->getOmschrijving().' : '.$ministrytree->getOmschrijving()."; eigen:".$ministrytree->getEigen()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetMinistryTree taken:'.$sub['taken']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			$this->GetTasks($ministrytree,$sub['taken']);
			$this->GetSubMinistries($ministrytree);
			$owner->addSubministry($ministrytree);
		}
		return;
	}
		
   /**
     * Fill tasks of $ministry
     *
	 * @param \Parousia\Churchtakenreg\Domain\Model\Ministrytree $ministrytree
	 * $taken string
     * @return void
    */
	function GetTasks($ministrytree,$taken)
	{	
		if (!empty($taken))
		{
			$aTask=explode('#%#',$taken);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetTasks aTask:'.urldecode(http_build_query($aTask,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');				
			foreach ($aTask as $taskrow)
			{
				$tasktree = new Tasktree();
				$task=explode('#$#',$taskrow);
				$tasktree->setUid($task[0]);
				$tasktree->setOmschrijving($task[1]);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetTasks taakomschrijving:'.$tasktree->getOmschrijving()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');				
				$workersstring=$task[2];
				if (!empty($workersstring))
 				{
					$aWorkers=explode('%#%',$workersstring);
					foreach ($aWorkers as $workerrow)
					{
						$worker=explode('%$%',$workerrow);
						$workertree=new Workertree();
						$workertree->setUid($worker[0]);
						$workertree->setFullname($worker[1]);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetTasks worker fullname:'.$workertree->getFullname()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');				
						$tasktree->addWorker($workertree);
					}
				}
				$ministrytree->addTask($tasktree);
			}
		}

	}
	
	function viewMinistryTree($uid = 0) {
		$this->antwoord='';
  		$this->GetTree($uid);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ViewMinistryTree ministrytree : '.urldecode(http_build_query($ministrytree,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

    	$assignedValues = [
		'top' => "0_a",
		'eersteafd' => $this->eersteafd,
		'ministrytree' => $this->ministrytree,
		'bedieningEdit' => $this->bedieningEdit,
		'exportpersons' => $this->exportpersons,
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ViewMinistryTree assignedValues : '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$this->antwoord= FluidTemplate::render("Ministry/ViewMinistryTree.html", $assignedValues,$this,'churchtakenreg') ;
		
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ViewMinistryTree out : '.$this->antwoord."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');		
	}
	
	function vieweditMinistry($ministryuid='',$eigenbediening) {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ViewEditMinistry eigenbediening : '.$eigenbediening.'; bedieningEdit: '.$this->bedieningEdit."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$id=intval(explode('_',$ministryuid)[0]);
		$ministry=$this->getMinistrySingle($id);
		$omschrijving=$ministry->getOmschrijving();
		$cleanteam=strpos($omschrijving,'cleanteam wk');
		if (($this->bedieningEdit or $eigenbediening) && ($cleanteam===false or $this->cleanteamsbeheer=='1'))$editpermission=1;else $editpermission=0;
		$stack[]=array("action"=>"showhierarchy","controller"=>"Ministry","extensionName"=>"churchtakenreg","pluginName"=>"ministry","pageUid"=>$this->conf['pageId'],"ministryid"=>$ministryuid);


    	$assignedValues = [
			'ministry' => $ministry,
			'ministryuid'=>$ministryuid,
			'stack'=>json_encode($stack),
			'editpermission' => $editpermission,
			'eigenbediening' => $eigenbediening,
			'lang' => $this->conf['lang'],
			'pagemailen' => $this->conf['pagemailen'],
			'parentuid' => '',
             ];
		//$out= $this->renderFluidTemplate("ViewEditMinistry.html", $assignedValues) ;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'vieweditMinistry assignedValues : '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$this->antwoord= FluidTemplate::render("Ministry/ViewEditMinistry.html", $assignedValues,$this,'churchtakenreg','churchpersreg') ;

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ViewEditMinistry antwoord : '.$this->antwoord."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		//$this->antwoord .=   $out;
		return;
	}	
	
	function cancelMinistry() {
		header("SAVE_SUCCESS: 2");
	}	

	function deleteMinistry($Ministryuid='') {
		// delete Ministry when not used anymore:
		$id=intval(explode('_',$Ministryuid)[0]);
		$query="delete from bediening where uid=".$id;
		$result=$this->db->query($query);

		header("SAVE_SUCCESS: 1"); 
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete ministry query : '.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
}

	function saveMinistry($Ministryuid='') {
		// delete Ministry when not used anymore:
		$id=intval(explode('_',$Ministryuid)[0]);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save ministry Ministryuid : '.$Ministryuid."; id:".$id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$ids=explode(',',str_replace(',0','',$this->aParms['id_bedieningsleider']));
		$ids=array_unique($ids);
				
		$query="update bediening set omschrijving='".$this->db->real_escape_string($this->aParms['omschrijving'])."'".
		", id_bedieningsleider='".implode(',',$ids)."'".
		",secretariaat='".$this->aParms['secretariaat']."'".
		",soort='".$this->aParms['soort']."'".
		",taakvisie='".$this->db->real_escape_string($this->aParms['taakvisie'])."'".
		",toekomstverwachting='".$this->db->real_escape_string($this->aParms['toekomstverwachting'])."'".
		",planactiviteiten='".$this->db->real_escape_string($this->aParms['planactiviteiten'])."'".
		",volgendejaren='".$this->db->real_escape_string($this->aParms['volgendejaren'])."'".
		" where uid='".$id."'";
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save ministry query : '.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$result=$this->db->query($query);


		//header("SAVE_SUCCESS: 1"); 
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save ministry omschrijving : '.$omschrijving."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$data= array(
		'soort'=>$this->aParms['soort'],
		'omschrijving'=>$this->aParms['omschrijving']);
		$this->antwoord .=json_encode($data);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save ministry out : '.$this->antwoord."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

}

	function viewaddMinistry($parentuid='') {
		$ministry= new Ministry();
		$ministry->setSoort('bediening');
		if (!empty($parentuid))
		{
			$id=intval(explode('_',$parentuid)[0]);
			$query="select omschrijving from bediening where uid=".$id." limit 1";
			$result=$this->db->query($query);
			$row=$result->fetch_array(MYSQLI_ASSOC);
			$ministry->setParentdepartment($row['omschrijving']);
		}
		if ($this->bedieningEdit or $eigenbediening && (strpos($ministry->getParentdepartment(),'cleanteam wk')!==0 or $this->cleanteamsbeheer=='1'))$editpermission=1;else $editpermission=0;
    	$assignedValues = [
			'ministry' => $ministry,
			'parentuid'=>$parentuid,
			'lang' => $this->conf['lang'],
             ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewaddMinistry parent: '.$ministry->getParentdepartment().'; assignedValues : '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		//$out= $this->renderFluidTemplate("ViewEditMinistry.html", $assignedValues) ;
		$this->antwoord= FluidTemplate::render("Ministry/ViewEditMinistry.html", $assignedValues,$this,'churchtakenreg','churchpersreg') ;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ViewEditMinistry out : '.$out."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		return;
	}	

	function savenewMinistry($parentuid='') {
		// delete Ministry when not used anymore:
		$id=intval(explode('_',$parentuid)[0]);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save ministry Ministryuid : '.$Ministryuid."; id:".$id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$query="select count(*) as cnt from bediening where id_parent=".$id.' and omschrijving="'.$this->db->real_escape_string($this->aParms['omschrijving']).'"';
		$result=$this->db->query($query);
		$row=$result->fetch_array(MYSQLI_ASSOC);
		$cnt=$row['cnt'];
		
		if ($cnt>0)
		{
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savenewTask error : Er is al een taak '.$omschrijving.' onder deze bediening:'.$Ministryuid."; id:".$id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			$this->antwoord.= "error: Er is al een bediening '".$this->aParms['omschrijving']."' onder deze afdeling";
			return;
		}
		
		$ids=explode(',',str_replace(',0','',$this->aParms['id_bedieningsleider']));
		$ids=array_unique($ids);
		$query="insert into bediening set omschrijving='".$this->db->real_escape_string($this->aParms['omschrijving'])."'".
		", id_parent=".$id.
		", id_bedieningsleider='".implode(',',$ids)."'".
		",secretariaat='".$this->aParms['secretariaat']."'".
		",soort='".$this->aParms['soort']."'".
		",taakvisie='".$this->db->real_escape_string($this->aParms['taakvisie'])."'".
		",toekomstverwachting='".$this->db->real_escape_string($this->aParms['toekomstverwachting'])."'".
		",planactiviteiten='".$this->db->real_escape_string($this->aParms['planactiviteiten'])."'".
		",volgendejaren='".$this->db->real_escape_string($this->aParms['volgendejaren'])."'";
		$result=$this->db->query($query);

		$this->viewMinistryTree($id);
}

function viewTask($taskuid='',$eigenbediening) {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewTask taskuid: '.$taskuid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$id=intval(explode('_',$taskuid)[0]);
		$GIFTS0='';
		$GIFTS1='';
		$GIFTS2='';
		$task=$this->getTaskSingle($id);

		$this->a_gaven=churchgiftsreg_div::$a_gaven;
		array_unshift($this->a_gaven,"");
		$aGaven=explode(',',$task->getGaven());
		for ($i=0;$i<3;$i++)
		{
			$giftsname="GIFTS".$i;
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."viewTask bepaal giftsname ".$giftsname."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			if ($i<count($aGaven))
				$$giftsname=churchgiftsreg_div::OptgroupList($aGaven[$i],$this->a_gaven,true);		
			else
				$$giftsname=churchgiftsreg_div::OptgroupList(array(),$this->a_gaven,true);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."viewTask ".$giftsname.": ".$$giftsname."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
					
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."viewTask a_gaven: ".http_build_query($this->a_gaven,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');		
		$GEESTRIJP=churchgiftsreg_div::OptgroupList(array($task->getGeestelijkerijpheid()),$this->a_rijpheid);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."viewTask GEESTRIJP: ".$GEESTRIJP."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$omschrijving=$task->getOmschrijving();
		$cleanteam=strpos($omschrijving,'cleanteam wk');
		if (($this->bedieningEdit or $eigenbediening) && ($cleanteam===false or $this->cleanteamsbeheer=='1'))$editpermission=1;else $editpermission=0;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."viewTask editpermission: ".$editpermission."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$stack[]=array("action"=>"showhierarchy","controller"=>"Ministry","extensionName"=>"churchtakenreg","pluginName"=>"ministry","pageUid"=>$this->conf['pageId'],"ministryid"=>$taskuid);
    	$assignedValues = [
			'task' => $task,
			'taskuid'=>$taskuid,
			'stack'=>json_encode($stack),
			'editpermission' => $editpermission,
			'eigenbediening' => $eigenbediening,
			'GIFTS1'=>$GIFTS0,
			'GIFTS2'=>$GIFTS1,
			'GIFTS3'=>$GIFTS2,
			'GEESTRIJP'=>$GEESTRIJP,
			'lang' => $this->conf['lang'],
			'pagemailen' => $this->conf['pagemailen'],
             ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewTask assignedValues : '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$this->antwoord= FluidTemplate::render("Ministry/ViewEditTask.html", $assignedValues,$this,'churchtakenreg','churchpersreg') ;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewTask antwoord : '.$this->antwoord."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		return;

	}	

	function viewaddTask($parentuid='') {
		$task=new Task();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewaddTask parentuid : '.$parentuid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		if (!empty($parentuid))
		{
			$id=intval(explode('_',$parentuid)[0]);
			$query="select omschrijving,soort from bediening where uid=".$id." limit 1";
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewaddTask query : '.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			$result=$this->db->query($query);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewaddTask error : '.$this->db->error.'; result: '.urldecode(http_build_query($result,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			
			$row=$result->fetch_array(MYSQLI_ASSOC);
			$task->setParentdepartment($row['omschrijving']);
			$task->setSoort($row['soort']);
		}
		
		$this->a_gaven=churchgiftsreg_div::$a_gaven;
		array_unshift($this->a_gaven,"");
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF']."viewaddTask a_gaven: ".http_build_query($this->a_gaven,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		for ($i=0;$i<3;$i++)
		{
			$giftsname="GIFTS".$i;
			$$giftsname=churchgiftsreg_div::OptgroupList('',$this->a_gaven,true);
		}
		$GEESTRIJP=churchgiftsreg_div::OptgroupList('',$this->a_rijpheid);
    	$assignedValues = [
			'task' => $task,
			'parentuid'=>$parentuid,
			'GIFTS1'=>$GIFTS0,
			'GIFTS2'=>$GIFTS1,
			'GIFTS3'=>$GIFTS2,
			'GEESTRIJP'=>$GEESTRIJP,
 			'lang' => $this->conf['lang'],
            ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewaddTask assignedValues : '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$this->antwoord= FluidTemplate::render("Ministry/ViewEditTask.html", $assignedValues,$this,'churchtakenreg') ;
		return;

}

	function cancelTask() {
		header("SAVE_SUCCESS: 2");
	}	

	function deleteTask($Taskuid='') {
		// delete Ministry when not used anymore:
		$id=explode('_',$Taskuid)[0];
		$query="delete from taak where uid=".intval($id);
		$result=$this->db->query($query);

		header("SAVE_SUCCESS: 1"); 
}

	function saveTask($Taskuid='') {
		$id=explode('_',$Taskuid)[0];
		//$omschrijving=strip_tags($this->aParms['omschrijving']);
		$query="update taak set omschrijving='".$this->db->real_escape_string($this->aParms['omschrijving'])."'".
		", verlangen='".$this->db->real_escape_string($this->aParms['verlangen'])."'".
		",gaven='".$this->aParms['gaven']."'".
		",energiestijl='".$this->aParms['energiestijl']."'".
		",organisatiestijl='".$this->aParms['organisatiestijl']."'".
		",vaardigheden='".$this->db->real_escape_string($this->aParms['vaardigheden'])."'".
		",verplichtingen='".$this->db->real_escape_string($this->aParms['verplichtingen'])."'".
		",geestelijke_rijpheid='".$this->db->real_escape_string($this->aParms['geestelijke_rijpheid'])."'".
		",lidmaatschap='".$this->db->real_escape_string($this->aParms['lidmaatschap'])."'".
		",aantal='".intval($this->aParms['aantal'])."'".
		",duur='".intval($this->aParms['duur'])."'".
		",tijdsbeslag='".$this->db->real_escape_string($this->aParms['tijdsbeslag'])."'".
		",opmerkingen='".$this->db->real_escape_string($this->aParms['opmerkingen'])."'".
		" where uid='".intval($id)."'";
		$result=$this->db->query($query);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveTask taak error: '.$this->db->error.'; affected:'.$this->db->affected_rows.'; query:'.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
					
		$data= array(
		'omschrijving'=>$this->aParms['omschrijving']);
		$this->antwoord .=json_encode($data);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save ministry out : '.$this->antwoord."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

}


	function savenewTask($parentuid='') {
		$id=explode('_',$parentuid)[0];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ssavenewTask Ministryuid : '.$parentuid."; id:".$id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		// check if omschrijving already present with this ministry:
		$query='select count(*) as cnt from taak where id_parent="'.$id.'" and omschrijving="'.$this->db->real_escape_string($this->aParms['omschrijving']).'"';
		$result=$this->db->query($query);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savenewTask heck if omschrijving already present error: '.$this->db->error.'; affected:'.$this->db->affected_rows.'; query:'.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$row=$result->fetch_assoc();
		$cnt=$row['cnt'];

		if ($cnt>0)
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savenewTask error : Er is al een taak '.$omschrijving.' onder deze bediening:'.$Ministryuid."; id:".$id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			$this->antwoord.= "error: Er is al een taak '".$this->aParms['omschrijving']."' onder deze bediening";
			return;
		}
		array_walk($this->aParms,function(&$item){$item=strval($item);});
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ssavenewTask aParms : '.urldecode(http_build_query($this->aParms,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

		$query="insert into taak set omschrijving='".$this->db->real_escape_string($this->aParms['omschrijving'])."'".
		", id_parent =". intval($id).
		", verlangen='".$this->db->real_escape_string($this->aParms['verlangen'])."'".
		",gaven='".$this->aParms['gaven']."'".
		",energiestijl='".$this->aParms['energiestijl']."'".
		",organisatiestijl='".$this->aParms['organisatiestijl']."'".
		",vaardigheden='".$this->db->real_escape_string($this->aParms['vaardigheden'])."'".
		",verplichtingen='".$this->db->real_escape_string($this->aParms['verplichtingen'])."'".
		",geestelijke_rijpheid='".$this->db->real_escape_string($this->aParms['geestelijke_rijpheid'])."'".
		",lidmaatschap='".$this->db->real_escape_string($this->aParms['lidmaatschap'])."'".
		",aantal='".intval($this->aParms['aantal'])."'".
		",duur='".intval($this->aParms['duur'])."'".
		",tijdsbeslag='".$this->db->real_escape_string($this->aParms['tijdsbeslag'])."'";
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savenewTask query:'.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$result=$this->db->query($query);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savenewTask error: '.$this->db->error.'; affected:'.$this->db->affected_rows.'; query:'.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

		$this->viewMinistryTree($id);
		",opmerkingen='".$this->db->real_escape_string($this->aParms['opmerkingen'])."'";

		
}

function dropTaak($id=0,$destid=0,$soort='')
{
	$query="update ";
	if ($soort=="a" or $soort=="b"){$query.='bediening';}
	elseif ($soort=="t"){$query.='taak';}
	else{$query.='taakbekleding';}
	$query.=" set id_parent='".$destid."' where uid='".$id."'";
	//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'droptaak query action : '.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
	
	$this->db->query($query);
	header("SAVE_SUCCESS: 1"); 

}


function viewWorker($workeruid='',$eigenbediening) {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewWorker workeruid: '.$workeruid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$id=explode('_',$workeruid)[0];
		$coworker=$this->getCoWorkerSingle($id);

//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewWorker id:'.$id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		if ($this->bedieningEdit or $eigenbediening )$editpermission=1;else $editpermission=0;
		$date= new \DateTime("+60 days");
		$maxdate=$date->format('Y-m-d');
		$date= new \DateTime("1977-09-01");
		$mindate=$date->format('Y-m-d');

    	$assignedValues = [
			'coworker' => $coworker,
			'workeruid'=> $workeruid,
			'editpermission' => $editpermission,
			'datemin' => $mindate,
			'datemax' => $maxdate,
			'lang' => $this->conf['lang'],
             ];
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewTask assignedValues : '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$this->antwoord= FluidTemplate::render("Ministry/ViewEditCoworker.html", $assignedValues,$this,'churchtakenreg','churchpersreg') ;
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ViewEditMinistry out : '.$this->antwoord."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		return;

	}	

	function viewaddWorker($parentuid='') {
		$coworker=new Coworker();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewaddTask parentuid : '.$parentuid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		if (!empty($parentuid))
		{
			$id=intval(explode('_',$parentuid)[0]);
			$query="select omschrijving as parentname from taak where uid=".$id." limit 1";
			$result=$this->db->query($query);
			$row=$result->fetch_array(MYSQLI_ASSOC);
			$coworker->setParenttaskname($row['parentname']);
		}
		$date= new \DateTime("+60 days");
		$maxdate=$date->format('Y-m-d');
		$date= new \DateTime("1977-09-01");
		$mindate=$date->format('Y-m-d');

    	$assignedValues = [
			'coworker' => $coworker,
			'parentuid'=> $parentuid,
			'datemin' => $mindate,
			'datemax' => $maxdate,
 			'lang' => $this->conf['lang'],
            ];
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewTask assignedValues : '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$this->antwoord= FluidTemplate::render("Ministry/ViewEditCoworker.html", $assignedValues,$this,'churchtakenreg','churchpersreg') ;
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ViewEditMinistry out : '.$this->antwoord."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		return;

}

	function saveWorker($Workeruid='') {
		$id=explode('_',$Workeruid)[0];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save worker $Workeruid : '.$Workeruid."; id:".$id.'; opmerkingen:'.$this->aParms['opmerkingen']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		
		if (!$this->checkstartdate($this->aParms['datum_start']))
		{
		    $this->antwoord .= "error: ".$this->ErrMsg;
		    return;
		}
		
		// bijwerken ervaring oorspronkelijk persoon;
		$id_persoon_org=$this->bijwerkenervaring($id);		
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveWorker na bijwerken id_persoon:'.$id_persoon_org."; id:".$id.'; nw persoon:'.$this->aParms['id_persoon']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		if ($id_persoon_org<>$this->aParms['id_persoon'])
		{
			// Controleer of persoon al voorkomt bij de taak:
			$where='uid<>"'.$id.'" and id_persoon="'.$this->aParms['id_persoon'].'" and id_parent in (select id_parent from taakbekleding where uid="'.$id.'")';
			$query='select count(*) as cnt from taakbekleding where '.$where;
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveWorker taakbekeleding bestaat query:'.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			$result=$this->db->query($query);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveWorker taakbekeleding na query error: '.$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			$row=$result->fetch_assoc();
			$cnt=$row['cnt'];
			if ($cnt>0)
			{
	//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savenewTask error : Er is al een taak '.$omschrijving.' onder deze bediening:'.$Ministryuid."; id:".$id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
				$this->antwoord.= "error: Deze persoon komt al voor bij deze taak";
				return;
			}
		}
		$query='update taakbekleding set id_persoon='.intval($this->aParms['id_persoon']).',datum_start="'.$this->aParms['datum_start'].'",opmerkingen=AES_ENCRYPT("'.$this->aParms['opmerkingen'].'",@password) where uid='.intval($id);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save worker query : '.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$result=$this->db->query($query);
		// bijwerken ervaring huidig persson;
		if ($id_persoon_org<>$this->aParms['id_persoon']) $this->bijwerkenervaring($id);
	}


	function savenewWorker($parentuid='') {
		$id=explode('_',$parentuid)[0];
		// Controleer of persoon al voorkomt bij de taak:
		$query='select count(*) as cnt from taakbekleding where id_parent="'.$id.'" and id_persoon="'.$this->aParms['id_persoon'].'"';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savenewWorker taakbekeleding bestaat query:'.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$result=$this->db->query($query);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savenewWorker taakbekeleding na query error: '.$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$row=$result->fetch_assoc();
		$cnt=$row['cnt'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savenewWorker taakbekeleding bestaat al? :'.$cnt.' ;voor id_persoon:'.$this->aParms['id_persoon']."; error:".$this->db->error.'; query:'.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

		if ($cnt>0)
		{
		    //			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savenewTask error : Er is al een taak '.$omschrijving.' onder deze bediening:'.$Ministryuid."; id:".$id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		    $this->antwoord.= "error: Deze persoon komt al voor bij deze taak";
		    return;
		}
		if (!$this->checkstartdate($this->aParms['datum_start']))
		{
		    $this->antwoord .= "error: ".$this->ErrMsg;
		    return;
		}
		    
		$query="insert into taakbekleding set id_persoon=".intval($this->aParms['id_persoon']).",id_parent=".intval($id).',datum_start="'.$this->aParms['datum_start'].'",opmerkingen=AES_ENCRYPT("'.$this->aParms['opmerkingen'].'",@password)';
		$result=$this->db->query($query);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savenewWorker insert taakbekeleding na query:'.$query.': error: '.$this->db->error.'; aantal:'.$this->db->affected_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		
		// bijwerken ervaring persson;
		$id_tb=$this->db->insert_id;
		
		$this->bijwerkenervaring($id_tb);
		// get parent ministry:
		$query='select id_parent from taak where uid="'.$id.'"';
		$result=$this->db->query($query);
		$row=$result->fetch_assoc();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'savenewWorker get aprent minsitry query:'.$query.': error: '.$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$this->viewMinistryTree($row['id_parent']);

		return;
		
    }
	
	function exportBediening($id_bediening='',$type='',$username='')
	{
		$exportnaam="exportbediening".(($this->soort=="ldr")?"sleiders":"");
		$this->aExport=array();
		$this->soort=$type;

		if ($this->soort=='taak')
		{	
			$id_taak=$id_bediening;
			// get corresponding bediening
			$query='select id_parent,omschrijving from taak where uid="'.$id_taak.'"';
			$result = $this->db->query($query) or die("Can't perform Query: $query");	
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$id_bediening=$row["id_parent"];
			$taak=$row["omschrijving"];
		}
		
		// Haal omschrijving van $id_bediening:
		$query	=	'select uid, omschrijving, soort'.
					" from bediening".
					" where uid='".$id_bediening."'";
		$result = $this->db->query($query) or die("Can't perform Query: $query");	
		$row = $result->fetch_array(MYSQLI_ASSOC);
		$bediening=$row["omschrijving"];
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'exportBediening omschrijving:'.$bediening."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		
		// Zoek top-afdeling:
		if ($this->soort=="ldr")
			$this->HaalBedieningsLeiders($id_bediening,true);
		elseif ($this->soort=='taak')
		{
		//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'exportBediening Haalmedewerker:'.$bediening.'; id_taak:'.$id_taak.'; taak:'.$taak."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			$this->HaalMedewerker($id_taak, $bediening,$taak);
		}
		else
		{
			$this->HaalTaak($id_bediening, $bediening);
			$this->HaalBediening($id_bediening);
		}
		//if (is_array($this->aExport))error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'exportBediening aExport : '.http_build_query($this->aExport,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		
		//$this->aExport[]=["Naam"=>"jan willemsen","Adres"=>"","Postcode"=>"2716 LR","Woonplaats"=>"Zoetermeer","Tel.nummer"=>"079-3229184","E-mailadres"=>"karelkuijpers@gmail.com"];
		if (count($this->aExport)>0)
		{
			if ($this->soort<>"pertaak")
			{
				usort($this->aExport, function ($a, $b) {
				
				$res= strcasecmp($a['naam'], $b['naam']);
				return $res;
				});
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'exportBediening aExport na usort : '.GeneralUtility::array2xml($this->aExport)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			}
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'exportBediening aExport sorted : '.http_build_query($this->aExport,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			$type="medewerkers per taak";
			if ($this->soort=="alle" or $this->soort=="taak") $type="alle medewerkers";
			elseif ($this->soort=="ldr") $type="bedieningsleiders";
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'exportBediening exportnaam: '.$exportnaam.'; type:'.$type."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

        	$exportfilenaam=churchpersreg_div::ExportToXlsx($this->aExport,$exportnaam,$type,'',$username);
	//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'exportBediening exportfilenaam: '.$exportfilenaam."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			$data=array('status'=>'success','exportnaam'=>'/'.$exportfilenaam);
		}
		else
		{
			$data=array('status'=>'error','exportnaam'=>$exportnaam);
		}

		//ob_clean();
		//flush();
		$this->antwoord.= json_encode($data);
		
	
	}

	function cmp ($a, $b) { 
	   return strcasecmp($a[0], $b[0]); 
	} 
	 
	
	function HaalBediening($id_owner)
	{	
		$query	=	'select uid, omschrijving, soort'.
					" from bediening".
					" where id_parent='".$id_owner."' order by omschrijving";
		$result = $this->db->query($query) or die("Can't perform Query: $query");	
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) 
		{ 
			$id_bediening	= $row["uid"];
			$this->HaalBediening($id_bediening);
			$this->HaalTaak($id_bediening, $row["omschrijving"]);
		}
		$result->free(); 
	}
	
	function HaalTaak($id_bediening, $bediening)
	{	// haal taken bij een bediening op:
		$query	=	'select uid, id_parent, omschrijving'.
					" from taak".
					" where id_parent='".$id_bediening."'";
		$result = $this->db->query($query)  or die("Can't perform Query: $query");	
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) 
		{ 	$this->HaalMedewerker($row["uid"], $bediening, $row["omschrijving"]);}
		$result->free(); 
	}

	function HaalMedewerker($id_taak, $bediening,$taak)
	{	// haal medewerkers bij een taak op:
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'exportbediening haalmedewerker: bed:'.$bediening."; taak:".$taak.'; idbediening:'.$id_bediening."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$query = 'SELECT DISTINCT concat(achternaam,", ",roepnaam," ", tussenvoegsel) as naam'.
		', AES_DECRYPT(emailadres,@password) as emailadres'.
		', straatnaam,huisnummer,postcode,woonplaats'.
		', AES_DECRYPT(telefoonnr_vast,@password) as telefoonnr_vast'.
		', AES_DECRYPT(mobieltelnr,@password) as mobieltelnr'.
		' FROM persoon as tp, adres as ta, taakbekleding as mw '.
		' where AES_DECRYPT(tp.id_adres,@password)=ta.uid '.
		' and mw.id_persoon=tp.uid and mw.id_parent='.$id_taak.
		" and tp.deleted=0 and ta.deleted=0 ".
		" ORDER BY achternaam, roepnaam"; 
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'exportbediening haalmedewerker:'.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
	
		$result = $this->db->query($query) or die("Can't perform Query: $query");	
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'exportbediening haalmedewerker error:'.$this->db->error.'; aantal:'.$result->num_rows ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
	
		while ($row = $result->fetch_array(MYSQLI_ASSOC))  
		{	
			if ($this->soort!="alle") 
			{
				$row["bediening"]=$bediening;
				$row["taak"]=$taak;
			}
			array_push($this->aExport,$row);
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'exportbediening haalmedewerker aExport:'.http_build_query($this->aExport,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$result->free(); 
	}

	function HaalBedieningsLeiders($id_owner,$top=false)
	{	
		// select bedieningen:
	//	$query= "select b.id,concat(if(soort='afdeling','afdeling ',''),omschrijving) as bediening,b.id_bedieningsleider from bediening b where id_parent=".$id_owner." and b.id_bedieningsleider>'0'";
		$query= "select b.uid,concat(if(soort='afdeling','afdeling ',''),omschrijving) as bediening,b.id_bedieningsleider from bediening b where ".($top?"b.uid=":"b.id_parent=").$id_owner;
		
	
		$resultbed = $this->db->query($query) or die("Can't perform Query: $query");	
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'exportbediening HaalBedieningsLeiders id error:'.$this->db->error.'; aantal:'.$resultbed->num_rows.'; query:'.$query ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		while ($bed = $resultbed->fetch_array(MYSQLI_ASSOC))  
		{	
			// zoek bijbehorende personen:
			$coords=$bed['id_bedieningsleider'];
			if ($coords>'0')
			{
				$query	=	'select concat(achternaam,", ",roepnaam," ", tussenvoegsel) as naam'.
				', straatnaam,huisnummer,postcode,woonplaats'.
				', AES_DECRYPT(emailadres,@password) as emailadres'.
				', AES_DECRYPT(telefoonnr_vast,@password) as telefoonnr_vast'.
				', AES_DECRYPT(mobieltelnr,@password) as mobieltelnr'.
				" from persoon as p,adres as a ".
				" where p.uid in (".$coords.") and p.deleted=0 and".
				" AES_DECRYPT(p.id_adres,@password)=a.uid";
				//Trace("haal bedieningsleiders van ".$bed['bediening']." en pids:".$coords." query:".$query);
		
				$result = $this->db->query($query) or die("Can't perform Query: $query");	
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'exportbediening HaalBedieningsLeiders pers error:'.$this->db->error.'; aantal:'.$result->num_rows.'; query:'.$query ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
	
				while ($row = $result->fetch_array(MYSQLI_ASSOC))  
				{	
					$row=preg_replace("/\r\n/"," ",$row);
					$row['bedieningen']=$bed['bediening'];
					array_push($this->aExport,array_values($row));
				}
	
			}
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'exportbediening HaalBedieningsLeiders aExport na 1 bdng:'.http_build_query($this->aExport,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			$this->HaalBedieningsLeiders($bed['uid']);
		}
	}
	

	function checkstartdate($startdate)
	{
	    $this->ErrMsg='';
	    if (!empty($startdate))
	    {
	        $start=\DateTime::createFromFormat("Y-m-d",$startdate);
	        //error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'checkstartdate startdate:'.$startdate."; start:".$start."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
	        
	        if (!is_object($start))
	        {
	            $this->ErrMsg="startdatum ".$startdate." is een ongeldige datum";
	            //error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'checkstartdate ongeldig:'.$startdate+"; msg:".$this->ErrMsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
	            return false;
	        }
	        //error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'checkstartdate start:'.$start->format("Y-m-d")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
	        
	        if ($start->format("Y-m-d")!==$startdate)
	        {
	            $this->ErrMsg="startdatum ".$startdate." is een ongeldige datum";
	            return false;
	        }
	        if ($start > new \DateTime("+60 days"))
	        {
	            $this->ErrMsg="startdatum kan niet te ver in de toekomst liggen";
	            return false;
	        }
	        if ($start < new \DateTime("1977-09-01"))
	        {
	            $this->ErrMsg="startdatum - ".$startdate." - te ver in het verleden";
	            return false;
	        }
	        //$startdate=$start->format("Y-m-d");
	    }
	    else
	    {
	        $this->ErrMsg="startdatum mag niet leeg zijn";
	        //error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'checkstartdate leeg:'.$startdate+"; msg:".$this->ErrMsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
	        return false;       
	    }
	    return true;
	}



	function deleteWorker($Workeruid='') {
		// delete worker:
		$id=explode('_',$Workeruid)[0];
		// bijwerken ervaring verwijderde medewerker;
		$this->bijwerkenervaring($id);
		
		$query="delete from taakbekleding where uid=".intval($id);
		$result=$this->db->query($query);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'deleteWorker QUERY:'.$query."; error:".$this->db->error.'; affected:'.$this->db->affected_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

		header("SAVE_SUCCESS: 1"); 
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete ministry query : '.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
	}


	function bijwerkenervaring($id_tb)
	{	//
		// bijwerken van "ervaring met" in bedieningsprofile bij verwijdern van een medewerker $id_tb uit een taak:
		// 
		//	parameters: $id_tb: id van betreffende taakbekleding
		//	returned: id_person
		//
		// lees oorspronkelijke taakbekleding:
	    
		$fields="tb.id_parent,tb.id_persoon,t.omschrijving";
		$tables="taakbekleding tb, taak t";
		$where='tb.uid="'.$id_tb.'" and tb.id_parent=t.uid';
		$query="select ".$fields." from ".$tables." where ".$where." limit 1";
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bijwerkenervaring lees oorspronkelijke taakbekleding query : '.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$result=$this->db->query($query);
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bijwerkenervaring lees oorspronkelijke taakbekleding error : '.$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		$row=$result->fetch_array(MYSQLI_ASSOC);
		$id_persoon=$row['id_persoon'];
		$id_parent=$row['id_parent'];
		$taak=$row['omschrijving'];
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bijwerkenervaring query : '.$query."; taak:".$taak ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');

		// lees huidige ervaring:
		$fields = "uid,AES_DECRYPT(ervaring,@password) as ervaringmet";
		$tables="bedieningsprofiel";
		$where="AES_DECRYPT(person_id,@password)='".$id_persoon."'";
		$query="select ".$fields." from ".$tables." where ".$where." limit 1";
		$result=$this->db->query($query);
		$row=$result->fetch_array(MYSQLI_ASSOC);
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bijwerken ervaring huidig:'.$query.';; aantal rows:'.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
		if (!empty($row))
		{
			$ervaring=$row["ervaringmet"];
			$id=$row["uid"];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bijwerken ervaring id_persoon:'.$id_persoon."; id_parent:".$id_parent."; taak:".$taak."; ervaringmet:".$ervaring."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			
			if (strpos(strtolower($ervaring),strtolower($taak))===false)
			{	if (strlen($ervaring)>0)$ervaring.=", ";
				$ervaring.=$taak;
				$query="update bedieningsprofiel set ervaring=AES_ENCRYPT('".$ervaring."',@password) where uid=".$id;
				$result=$this->db->query($query);
			//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'toevoegen ervaring:'.$query."; error:".$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
			}
		}
		else
		{
			// insert new bedieningsprofiel:
				$query="insert into bedieningsprofiel set person_id=AES_ENCRYPT('".$id_persoon."',@password),ervaring=AES_ENCRYPT('".$taak."',@password)";
				$result=$this->db->query($query);
			//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'toevoegen profiel met ervaring:'.$query."; error:".$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchtakenreg/Classes/Controller/debug.txt');
				
		}
		return $id_persoon;
	}
	

 /**
   * Multi-array search
   *
   * @param array $array (key=> {0=>value1,1=>value2,...}
   * @param array $search ({key=>value,})
   * @return array
   */
	function multi_array_search($array, $search)
	{
		// Create the result array
      	// Iterate over each search condition
    	// find keys in corresponding array element
		$result=array();
      	foreach ($search as $k => $v)
      	{
			if (!isset($array[$k])) return array();
			if (empty($result)) $result=array_keys($array[$k],$v);
			else
			{
				$uniquekeys=array_keys($array[$k],$v);
				$result=array_intersect($result,$uniquekeys);
			}
			if (empty($result)) return $result;
    	}
    	// Return the result array
    	return $result;
	}
	

	function HeeftPermissie($permissie_)
	{
		// ga na of aan ��n van de gevraagde permissies in $permissie_ is voldaan.
		$permissieNodigArray_ = explode(",",strtoupper($permissie_));
		$gevonden_=false;
		$permissie_array_=$this->permissie;
						
		foreach ($permissieNodigArray_ as $permissienodig)
		{	if (!empty($permissie_array_))
			{	foreach ($permissie_array_ as $permission)
				{	
					If (strcasecmp($permission,$permissienodig)==0 OR strcasecmp($permission,churchpersreg_div::ADMINFUNC)==0){$gevonden_=true;break 2;}
				}
			}
		}
		return $gevonden_;
	}
}
