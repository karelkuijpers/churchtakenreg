<?php
defined('TYPO3') or die();

/***************
 * Plugin
 */

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Churchtakenreg',
    'Ministry',
    'Show hierarchy'
);
